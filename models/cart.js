const fs = require('fs');
const path = require('path');

const p = path.join(
    path.dirname(process.mainModule.filename),
    'data',
    'cart.json'
  );

module.exports = class Cart{
    static addProduct(id , productPrice){
        // fetch the previous cart
        fs.readFile(p, (err, fileContent) => {
            let cart = { products: [], totalPrice : 0};
            if(!err){
                cart = JSON.parse(fileContent);
            }
            // Analyze the cart => Find Existing product
            const existingProductIndex = cart.products.findIndex(prod => prod.id === id);
            const existingProduct = cart.products[existingProductIndex];
            let upadtedProduct;
    
            // Add new product/increase Quantity
            if (existingProduct) {
                upadtedProduct = { ...existingProduct };
                upadtedProduct.qty = upadtedProduct.qty + 1;
                cart.products = [...cart.products];
                cart.products[existingProductIndex] = upadtedProduct;
            } else{
                upadtedProduct = { id: id, qty :1};
                cart.products = [...cart.products ,upadtedProduct]
            }
            cart.totalPrice = cart.totalPrice  + +productPrice;
            fs.writeFile(p, JSON.stringify(cart) , err => {
                console.log(err);
            })
        })
    }
    static deleteProduct(id, productPrice){
        fs.readFile(p, (err, fileContent) => {
            if(err){
                return;
            }
            const upadtedCart = { ...JSON.parse(fileContent)};
            const product = upadtedCart.products.find(prod => prod.id === id);
            if(!product){
                return;
            }
            const productQty = product.qty;
            upadtedCart.products = upadtedCart.products.filter(prod => prod.id !== id);
            upadtedCart.totalPrice = upadtedCart.totalPrice - productPrice * productQty;

            fs.writeFile(p, JSON.stringify(upadtedCart) , err => {
                console.log(err);
            })
        })

    }
    static getCart(cb){
        fs.readFile(p, (err, fileContent) => {
            const cart = JSON.parse(fileContent);
            if(err){
                cb(null);
            }else{
                cb(cart);
            }
        })
    }
}